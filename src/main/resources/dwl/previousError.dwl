%dw 2.0
output application/json
---
[
    {
      "errorTitle": "My Previous Error",
      "errorMessage": "Previous Error Message",
      "apiName": "otherApp",
      "version": "1.0.1",
      "developerMessage": "Previous Developer Message",
      "errorCode": 1234,
      "moreInfo": null,
      "correlationId": "e4e79880-a5dd-11ea-81ed-38f9d3d3296a",
      "validationErrors": null
    }
 ]