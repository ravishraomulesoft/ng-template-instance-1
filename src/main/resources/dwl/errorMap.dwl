%dw 2.0
output application/json
---
[
	{
      "errorType":"EXAMPLE:MAP",
      "errorHttpStatus":400,
      "errorCode":1234,
      "errorMessage": "Map Error Message",
      "errorDeveloperMessage":  "Map Error Developer Message",
      "errorMoreInfo": "More Info",
      "errorTitle": "Customer Error Map Error"
   }
]